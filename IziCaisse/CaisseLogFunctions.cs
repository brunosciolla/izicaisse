﻿using System;
using RGiesecke.DllExport;

namespace IziCaisse
{
    public static class CaisseLogFunctions
    {
        private static readonly ILogger logger = new CaisseLogger();

        public enum LogLevel
        {
            Trace,
            Debug,
            Info,
            Warn,
            Error,
            Fatal
        }

        [DllExport]
        public static void InitializeLogger(string releaseString, string shopId, string LogFilePath)
        {
            try
            {
                CaisseLogger.InitializeLogger(releaseString, shopId, LogFilePath);
                logger.Info("Initialisation du log réussie.");
            }
            catch (Exception ex)
            {
                CaisseLogger.LogManually(ex);
            }
        }

        [DllExport]
        public static void LogMessage(LogLevel level, string message)
        {
            switch (level)
            {
                case LogLevel.Trace:
                    logger.Trace(message);
                    break;
                case LogLevel.Debug:
                    logger.Debug(message);
                    break;
                case LogLevel.Info:
                    logger.Info(message);
                    break;
                case LogLevel.Warn:
                    logger.Warn(message);
                    break;
                case LogLevel.Error:
                    logger.Error(message);
                    break;
                case LogLevel.Fatal:
                    logger.Fatal(message);
                    break;
                default:
                    logger.Info(message);
                    break;
            }
        }

        [DllExport]
        public static void LogError(string message, string stackTrace)
        {
            logger.Error(new CaisseException(message, stackTrace));
        }
    }
}
