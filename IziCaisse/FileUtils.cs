﻿using System;
using System.IO;

namespace IziCaisse
{
    public class FileUtils
    {
        public static void TruncateFileToSize(string filePath, decimal sizeInMegaOctets)
        {
            var newSize = (int)Math.Floor(sizeInMegaOctets * 1024 * 1024);
            using (var memoryStream = new MemoryStream(newSize))
            {
                using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite))
                {
                    if (fileStream.Length < newSize) return;
                    fileStream.Seek(-newSize, SeekOrigin.End);
                    fileStream.CopyTo(memoryStream);
                    fileStream.SetLength(newSize);
                    fileStream.Position = 0;
                    memoryStream.Position = 0;
                    memoryStream.CopyTo(fileStream);
                }
            }
        }
    }
}
