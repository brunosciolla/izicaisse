﻿using System;
using RGiesecke.DllExport;

namespace IziCaisse
{
    public class SnackingFunctions
    {
        private static readonly ILogger logger = new CaisseLogger();

        [DllExport]
        public static void CheckSnaking()
        {
            try
            {
                // do it
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
    }
}
