﻿using NLog;
using NLog.Config;
using NLog.Targets;
using Sentry;
using System;
using System.IO;

namespace IziCaisse
{
    public class CaisseLogger : IziCaisse.ILogger
    {
        private const string TestsReleaseString = "iZiBio@devtests";
        private static NLog.ILogger logger;
        private static FileTarget fileTarget;

        public static void InitializeLogger(string releaseString, string shopId, string logFilePath)
        {
            if (logger == null)
            {
                logger = GetLogger(logFilePath, releaseString, shopId);
            }
        }

        public static void Shutdown()
        {
            LogManager.Flush();
            LogManager.Shutdown();
        }

        public static void LogManually(Exception ex)
        {
            var shortLog = "Erreur lors de l'initialisation du log\n" + "\nMessage ---\n" + ex.Message
                + "\nStackTrace ---\n" + ex.StackTrace + "\nTargetSite ---\n" + ex.TargetSite;
            File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ccb.log"), shortLog);
        }

        public void Trace(string message)
        {
            logger.Trace(message);
        }

        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Info(string message)
        {
            logger.Info(message);
        }

        public void Warn(string message)
        {
            logger.Warn(message);
        }

        public void Warn(Exception exception, string message)
        {
            logger.Warn(exception, message);
        }

        public void Error(string message)
        {
            logger.Error(message);
        }

        public void Error(Exception exception)
        {
            logger.Error(exception);
        }

        public void Error(Exception exception, string message)
        {
            logger.Error(exception, message);
        }

        public void Fatal(string message)
        {
            logger.Fatal(message);
        }

        private static NLog.ILogger GetLogger(string logFilePath, string releaseString, string shopId)
        {
            var config = new LoggingConfiguration();
            config.AddSentry(o =>
            {
                o.SendDefaultPii = true; // Send Personal Identifiable information like the username of the user logged in to the device
                o.IncludeEventDataOnBreadcrumbs = true; // Optionally include event properties with breadcrumbs
                o.ShutdownTimeoutSeconds = 5;
                o.AddTag("logger", "${logger}");
                o.AddTag("shop", shopId);
                o.Dsn = new Dsn("https://b196a1f8a9f34cfb85a7cd7d435722ab@sentry.io/3063650");
                o.Layout = "${message}";
                o.BreadcrumbLayout = "${logger}: ${message}";
                o.MinimumBreadcrumbLevel = LogLevel.Debug;
                o.MinimumEventLevel = LogLevel.Warn;
                o.Release = releaseString;
            });

            fileTarget = new FileTarget("logfile")
            {
                FileName = logFilePath,
                Layout = "${longdate}|${level:uppercase=true}|${logger}|${message} ${exception:format=ToString}"
            };

            try
            {
                File.AppendAllText(logFilePath, "\nFichier log configuré.");
                FileUtils.TruncateFileToSize(logFilePath, 5);
            }
            catch (Exception ex)
            {
                File.AppendAllText(Path.Combine(Path.GetDirectoryName(logFilePath), "caisse.log"), "\nLe fichier log refuse l'écriture.");
                File.AppendAllText(Path.Combine(Path.GetDirectoryName(logFilePath), "caisse.log"), "\n" + ex.Message);
            }

            config.AddTarget(fileTarget);
            config.AddRuleForAllLevels(fileTarget);

            LogManager.Configuration = config;

            return LogManager.GetLogger("iZiCaisse");
        }
    }
}
