﻿using System;
using System.Runtime.Serialization;

namespace IziCaisse
{
    [Serializable]
    public class CaisseException : Exception
    {
        public CaisseException(string message, string stackTrace) : base(message)
        {
            StackTrace = stackTrace;
        }

        public CaisseException() : base()
        {
            StackTrace = base.StackTrace;
        }

        public CaisseException(string message) : base(message)
        {
            StackTrace = base.StackTrace;
        }

        public CaisseException(string message, Exception innerException) : base(message, innerException)
        {
            StackTrace = base.StackTrace;
        }

        protected CaisseException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {
            StackTrace = base.StackTrace;
        }

        public override string StackTrace { get; }

        public override string ToString()
        {
            return base.ToString() + System.Environment.NewLine + StackTrace;
        }
    }
}
